package com.bertramtruong.GeoTag;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends FragmentActivity {
    private GoogleMap mMap;
    private final int REQUEST_SEARCH = 1;
    private MenuItem refreshMenuItem;
    private String lastSearch = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setUpMapIfNeeded();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_actions, menu);
        refreshMenuItem = menu.findItem(R.id.action_refresh);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Event that is called when an item on top bar is selected.
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search: // Search button
                // start the search activity
                Intent intent = new Intent(this, com.bertramtruong.GeoTag.SearchResultsActivity.class);
                startActivityForResult(intent, REQUEST_SEARCH);
                return true;
            case R.id.action_refresh: // Refresh button
                // only refresh if we had a previous search
                if (lastSearch != null) {
                    new LoadLatestTweets().execute(lastSearch);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Event that is called when an activity calls finish().
     * Used by the search activity to pass back the search term.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // don't process anything that wasn't OK
        if (resultCode != RESULT_OK) return;

        switch (requestCode) {
            case REQUEST_SEARCH:
                // get search term
                String searchTerm = data.getStringExtra("search_term");

                // load tweets in the background
                Log.w("app", "load tweets for " + searchTerm);
                new LoadLatestTweets().execute(searchTerm);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Initialize the map fragment.
     */
    private void setUpMapIfNeeded() {
        if (mMap != null) {
            return;
        }
        mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        if (mMap == null) {
            return;
        }
        // Initialize map options. For example:
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

        // show location
        mMap.setMyLocationEnabled(true);

        // set info window click handler
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                // check if snippet exists
                if (marker.getSnippet().isEmpty()) {
                    return;
                }

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(marker.getSnippet()));
                startActivity(browserIntent);
            }
        });
    }

    /**
     * Returns a list of Twitter coordinates.
     * ===================================================================================
     * NOTE: negeki.com is my server, it will return a list of coordinates in JSON format.
     * The returned list is "dummy data", due to the fact that Twitter has recently
     * changed the geolocation feature of tweets to opt-in.
     * ===================================================================================
     * @param hashTag
     * @return
     */
    private HashMap<String, LatLng> getTweetLocations(final String hashTag) {
        HashMap<String, LatLng> tweets = new HashMap<String, LatLng>();

        try {
            String json = com.bertramtruong.GeoTag.Helper.getJSON(String.format("https://negeki.com/twitter/search.php?q=%s", URLEncoder.encode(hashTag, "utf-8")));
            Log.w("app", json);
            JSONObject jsonObject = new JSONObject(json);

            JSONArray locations = jsonObject.getJSONArray("locations");

            // no results were returned
            if (locations.length() == 0) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getBaseContext(), "No results for " + hashTag + ".", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            // put the locations into an array to be processed
            for (int i = 0; i < locations.length(); i++) {
                JSONObject coordinates = locations.getJSONObject(i).getJSONObject("coordinates");
                String url = "";
                if (locations.getJSONObject(i).has("url")) {
                    url = locations.getJSONObject(i).getString("url");
                }
                tweets.put(url, new LatLng(coordinates.getDouble("latitude"), coordinates.getDouble("longitude")));
            }
        } catch (Exception x) {
            x.printStackTrace();
        }
        return tweets;
    }

    /**
     * Async task to load latest tweets
     */
    private class LoadLatestTweets extends AsyncTask<String, Void, HashMap<String, LatLng>> {
        private String search = "";

        @Override
        protected void onPreExecute() {
            refreshMenuItem.setActionView(R.layout.action_progressbar);
        }

        @Override
        protected HashMap<String, LatLng> doInBackground(String... strings) {
            try {
                // should never get more than 1 string
                for (String s : strings) {
                    this.search = s;
                    return getTweetLocations(s);
                }
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * After completing the task, display the markers on the map.
         * @param locations
         */
        @Override
        protected void onPostExecute(HashMap<String, LatLng> locations) {
            lastSearch = search;
            mMap.clear();
            for (Map.Entry<String, LatLng> kv : locations.entrySet()) {
                mMap.addMarker(new MarkerOptions()
                        .position(kv.getValue())
                        .title(search).snippet(kv.getKey()));
            }
            refreshMenuItem.setActionView(null);
        }
    }
}
