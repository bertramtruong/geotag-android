package com.bertramtruong.GeoTag;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by bertramtruong on 13/06/2014.
 */
public class Search implements Parcelable {

    private long id;
    private String text;

    public Search(long id, String search)
    {
        this.id = id;
        this.text = search;
    }

    public Search(Parcel in) {
        this.id = in.readLong();
        this.text = in.readString();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(text);
    }
}
