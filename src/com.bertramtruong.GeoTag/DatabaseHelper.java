package com.bertramtruong.GeoTag;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by bertramtruong on 13/06/2014.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    // Set database properties
    public static final String DATABASE_NAME = "GeoTagDB";
    public static final int DATABASE_VERSION = 1;

    // Constructor
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE searches (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, search TEXT NOT NULL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS searches");
        onCreate(db);
    }

    public void addSearch(String tag)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("search", tag);
        db.insert("searches", null, values);
        db.close();
    }

    public HashMap<Long, com.bertramtruong.GeoTag.Search> getSearches()
    {
        HashMap<Long, com.bertramtruong.GeoTag.Search> searches = new LinkedHashMap<Long, com.bertramtruong.GeoTag.Search>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM searches ORDER BY id DESC", null);
        if (cursor.moveToFirst()) {
            do {
                com.bertramtruong.GeoTag.Search search = new com.bertramtruong.GeoTag.Search(cursor.getLong(0), cursor.getString(1));
                searches.put(cursor.getLong(0), search);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return searches;
    }

    public void deleteSearch(com.bertramtruong.GeoTag.Search s) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("searches", "id = ?", new String[] { String.valueOf(s.getId()) });
        db.close();
    }

    public void clearSearches()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("searches", null, null);
        db.close();
    }
}