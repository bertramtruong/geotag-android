package com.bertramtruong.GeoTag;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.*;

import java.util.ArrayList;

/**
 * Created by bertramtruong on 29/04/2014.
 */
public class SearchResultsActivity extends Activity {
    private ActionBar actionBar;
    private ListView listResults;
    private com.bertramtruong.GeoTag.DatabaseHelper dbHelper;
    private ListAdapter adapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        actionBar = getActionBar();
        actionBar.setHomeButtonEnabled(true);

        // populate SQLITE
        dbHelper = new com.bertramtruong.GeoTag.DatabaseHelper(getApplicationContext());
        listResults = (ListView) findViewById(R.id.listSearchResults);
        final ArrayList<com.bertramtruong.GeoTag.Search> searches = new ArrayList<com.bertramtruong.GeoTag.Search>(dbHelper.getSearches().values());
        adapter = new com.bertramtruong.GeoTag.SearchAdapter(this, searches);
        listResults.setAdapter(adapter);

        // set onclick handler
        listResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                // get selected search
                com.bertramtruong.GeoTag.Search result = (com.bertramtruong.GeoTag.Search) listResults.getAdapter().getItem(i);
                passSearchTag(result.getText());
            }
        });

        // set onlongclick handler
        listResults.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setTitle("Delete search?");
                builder.setMessage("Are you sure you wish to delete this search term?");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // remove search
                        com.bertramtruong.GeoTag.Search s = searches.get(position);
                        Log.w("app", "" + s.getId());
                        dbHelper.deleteSearch(s);
                        refreshListView();
                        Toast.makeText(getBaseContext(), "Search has been removed.", Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Close the dialog
                        dialogInterface.cancel();
                    }
                });

                builder.create().show();
                return true; // no more further processing (ie. prevent onclick event)
            }
        });
    }

    private void refreshListView() {
        final ArrayList<com.bertramtruong.GeoTag.Search> searches = new ArrayList<com.bertramtruong.GeoTag.Search>(dbHelper.getSearches().values());
        adapter = new com.bertramtruong.GeoTag.SearchAdapter(this, searches);
        listResults.setAdapter(adapter);
    }

    private void passSearchTag(String tag) {
        // check for "hash"; add hash if not present
        if (tag.charAt(0) != '#') {
            tag = "#" + tag;
        }

        // save tag
        dbHelper.addSearch(tag);

        Log.w("app", "do search for " + tag);
        Intent intent = new Intent();
        intent.putExtra("search_term", tag);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_search, menu);

        // Expand edit text and focus on text box
        MenuItem menuItem = menu.findItem(R.id.search_action);
        menuItem.expandActionView();
        final EditText editText = (EditText) menuItem.getActionView().findViewById(R.id.menu_item_search);
        editText.requestFocus();

        // Set key listener to filter list
        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                String text = editText.getText().toString();
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyEvent.getKeyCode()) {
                        case KeyEvent.KEYCODE_ENTER:
                        case KeyEvent.KEYCODE_NUMPAD_ENTER:
                            // if empty string, do nothing
                            if (text.length() == 0) break;

                            // pass back to main activity to process
                            passSearchTag(text);
                            break;
                    }
                } else if (keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    ((com.bertramtruong.GeoTag.SearchAdapter) adapter).getFilter().filter(text);
                }
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.clear_action:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Delete all searches?");
                builder.setMessage("Are you sure you wish to delete all searches?");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dbHelper.clearSearches();
                        refreshListView();
                        Toast.makeText(getBaseContext(), "All searches cleared.", Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Close the dialog
                        dialogInterface.cancel();
                    }
                });

                builder.create().show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}