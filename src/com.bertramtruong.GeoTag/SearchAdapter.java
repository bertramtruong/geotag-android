package com.bertramtruong.GeoTag;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by bertramtruong on 13/06/2014.
 */
public class SearchAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private Filter mFilter;
    private ArrayList<com.bertramtruong.GeoTag.Search> searches;

    public SearchAdapter(Context context, ArrayList<com.bertramtruong.GeoTag.Search> searches) {
        this.context = context;
        this.searches = searches;
    }

    @Override
    public int getCount() {
        return searches.size();
    }

    @Override
    public Object getItem(int i) {
        return searches.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_search_item, null);
        }

        TextView searchText = (TextView)view.findViewById(R.id.searchText);

        // assign values
        searchText.setText(searches.get(i).getText());
        return view;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new SearchFilter(this.searches);
        }
        return mFilter;
    }

    private class SearchFilter extends Filter {
        ArrayList<com.bertramtruong.GeoTag.Search> original;

        public SearchFilter(ArrayList<com.bertramtruong.GeoTag.Search> original) {
            this.original = original;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                results.values = original;
                results.count = original.size();
                return results;
            }

            ArrayList<com.bertramtruong.GeoTag.Search> filtered = new ArrayList<com.bertramtruong.GeoTag.Search>();
            String filterString = constraint.toString().toLowerCase();

            Iterator<com.bertramtruong.GeoTag.Search> it = searches.iterator();
            while (it.hasNext()) {
                com.bertramtruong.GeoTag.Search s = it.next();
                if (s.getText().toLowerCase().contains(filterString)) {
                    filtered.add(s);
                }
            }

            results.values = filtered;
            results.count = filtered.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            if (filterResults.count > 0) {
                searches = (ArrayList<com.bertramtruong.GeoTag.Search>) filterResults.values;
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
